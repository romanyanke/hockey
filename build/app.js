var jsMin = 1
   ,cssMin = 1
   ,htmlMin = 1

process.env.NODE_ENV = cssMin ? 'production' : 'development'

var stylus = require('stylus'),
  connect = require('connect'),
  http = require('http'),
  connectAssets  = require('connect-assets'),
  connectJade  = require('connect-jade-html'),
  nib = require('nib'),
  port = 3000

var app = connect()
  .use(connect.logger('dev'))
  .use(connectAssets({
    src: __dirname + '/../src/assets/',
    buildDir: '/html/',
    build: jsMin,
    compress: true,
    detectChanges : true,
    buildFilenamer: function (a,b) { return a;}
  })) 
  .use(function(req, res, next){  
    if (req.url == '/') {
      res.statusCode = 302;       
      res.setHeader('Location', 'index.html');     
      res.end('Moved to ' + 'index.html');  
    }
    next();
  })
  .use(connectJade({
      src: __dirname + "/../src/views/",
      dest: __dirname + '/html',          
      pretty: !htmlMin,       
      force: true
    })
  ) 
  .use(connect.errorHandler())
  .use(connect.static(__dirname + '/html'))

    
app.listen(port);

console.log('ready:' + port);