//= require bootstrap.min

var teams;

$(function(){
  $('.action-type').each(function(){
    $(this).find('label').eq(0).button('toggle');
    // $(this).find('input').eq(0).attr('checked',true);
  });

  var controls = {
    btns: $('.actions').find('button'),
    on: function(){
      this.btns.attr('disabled', false);
    },
    off: function(){
      this.btns.attr('disabled', true);
    }
  };
  controls.off();

  teams = {
    team1: {
      name: 'Бешенные клюшки',
      score: 0,
      table: $('.team1-score')
    },
    team2: {
      name: 'Безбашенная шайба',
      score: 0,
      table: $('.team2-score')
    },
    scored:function( t ){
      var team = this['team'+t];
      team.score ++;
      team.table.html(team.score)
       // ev, team, player
      // log.insert_record('Гол', team.name, player);
    },
    reset: function(){
      if (confirm('Точно?')) {
        log.empty();
        timer.reset();
        this.team1.score = 0;
        this.team2.score = 0;
        this.team1.table.text(0);
        this.team2.table.text(0);
        period = 1;
        started = 0;
      }
    }
  };
  $('#reset').on('click', function(){
    teams.reset();
  });
    

  var period_time = 10;
  var period = 1;
  var started = 0;

  var timer = {
    obj: $('.time'),
    per: $('.period'),
    init: function(){

      this.per.text(period);
      this.obj.text( this.display( this.val ) )
    },
    elapsed: function(){
      var el = period_time - this.val;
      return this.display (el);
    },
    val: period_time,
    reset: function(){
      clearInterval(timer.interval);
      this.val = period_time;
      this.obj.text ( this.display(this.val) );
    },
    start: function(){
      this.interval = setInterval(function(){
        timer.val--;
        timer.obj.text( timer.display( timer.val ) )
        if(timer.val === 0){
          endPeriod();
        }
      }, 1000);
      controls.on();
    },
    pause: function(){
      // this.val--;
      controls.off();
      clearInterval(timer.interval);
    },
    display: function(sec){
      var s = sec % 60;
      var m = (sec - s) / 60;
      s = s < 10 ? '0'+s : s;
      m = m < 10 ? '0'+m : m;
      return m+':'+s;
    }
  };
  timer.init();

  var endPeriod = function(){
    // play('timesup');
    game.button('toggle');
    controls.off();
    timer.reset();
    if(period === 1){
      log.insert_row('Конец первого периода '+ teams.team1.score + ':' + teams.team2.score);
      period = 2;
      started = 0;
    } else if (period === 2){
      log.insert_row('Конец игры '+ teams.team1.score + ':' + teams.team2.score);
    }
  };

  var play = function( slug ){
    document.getElementById(slug).play();
  };

  var logged = $('#log');

  var log = {
    empty: function(){
      logged.empty();
    },
    header:['Событие','Время','Команда','Игрок'],
    render_header: function(){
      var html = '<tr>';
      for (var i=0; i<this.header.length; i++){
        html += '<th>' + this.header[i] + '</th>'
      }
      return html + '</tr>';
    },
    row: function(msg){
      return '<tr><td colspan="'+this.header.length+'"><strong>' + msg + '</strong></td></tr>';
    },
    record: function(ev, team, player){
      var r = '<tr>';
      r += '<td>' + ev + '</td>';
      r += '<td>' + timer.elapsed() + '</td>';
      r += '<td>' + team + '</td>';
      r += '<td>' + player + '</td>';
      return r + '</tr>';
    },
    insert_header: function(){
      logged.append(this.render_header());
    },
    insert_row: function(msg){
      logged.append( this.row(msg) );
    },
    insert_record: function( ev, team, player ){
      logged.append( this.record(ev, team, player) );
    }
  }


  var game = $('#game');

  game.on('click', function(){
    var playing = !game.hasClass('active');
    
    if(playing){
      if(started === 0){
        if(period === 1){
          log.insert_row(timestamp() + ' ' + teams.team1.name + ' vs '+ teams.team2.name);
          log.insert_row('Первый период');
        } else {
          log.insert_row('Второй период');
          timer.per.html(period);
        }

        log.insert_header();
        timer.start();
        started = 1;

      } else {
        timer.start();
      }

      // btn.text('Пауза')
    } else {
      timer.pause();
    }
  });

  $('.team-players').find('button').on('click', function(){
    // ev, team, player
    var btn = $(this);
    var team = btn.data('team');
    var ev = $('.action-type').filter('[data-team='+team+']').find('input:checked').val();
    log.insert_record(ev, teams['team'+team].name, btn.data('name'));
    if(ev === 'Гол'){
      teams.scored( team );
    } else if (ev === 'Автогол'){
      var t2 = team === 1 ? 2 : 1;
      teams.scored( t2 );
    }
  });
  $('.bonus').find('button').on('click', function(){
    var btn = $(this);
    var team = btn.data('team');
    var bonus = btn.data('bonus');
    log.insert_row(teams['team'+team].name + ': ' + bonus +'!')
  });



  var timestamp = function(){
    var date = new Date(),
    h = date.getHours(),  
    m = date.getMinutes(),
    d = date.getDate(),
    mn = date.getMonth()+1,
    h = (h < 10) ? '0' + h : h,  
    m = (m < 10) ? '0' + m : m,
    d = (d < 10) ? '0' + d : d,
    mn = (mn < 10) ? '0' + mn : mn;

    return d+'.'+mn+'.'+date.getFullYear() + ', ' + h + ':' + m;

    // $('.calendar .time').html(h + ':' + m);
    // $('.calendar .date').html(d+'.'+mn+'.'+date.getFullYear());
    // $('.calendar .day').html(function(){
    //   return ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота','воскресенье'][date.getDay()];
    // });
  };

});